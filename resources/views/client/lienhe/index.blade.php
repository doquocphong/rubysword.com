<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liên Hệ</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/LienHe.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid main1">
    <div class="container">
        <div class="row ly1">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.535490087685!2d105.79980511423629!3d21.011249286007875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aca08f967691%3A0xd0046c220f90264!2zMjcgxJDhu5cgUXVhbmcsIFRydW5nIEhvw6AsIFRoYW5oIFh1w6JuLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1617591342663!5m2!1svi!2s" width="1170" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
        <div class="row ly2">

            <div class="col col-md-6 col-12">
                <p>Công ty TNHH Tập Đoàn Ruby’s World là công ty chuyên sản xuất, gia công và phân phối mỹ phẩm. Với 2 thương hiệu mỹ phẩm cao cấp là Magic Skin & Magic Mom đã nhanh chóng có được vị trí khá cao trên thị trường nhờ dòng sản phẩm chăm sóc cơ thể, chất lượng tốt, mẫu mã đẹp, giá thành hợp lý, phù hợp với cơ địa của người Châu Á nói chung và người Việt Nam nói riêng.
                    <br> Công ty TNHH Tập Đoàn Ruby’s World
                    <br> VPGD: Số 27 Đỗ Quang, Trần Duy Hưng, Cầu Giấy, Hà Nội
                    <br> Địa chỉ nhà máy: KCN Thạch Thất, thôn Ngô Sài, huyện Quốc Oai, TP. Hà Nội
                    <br>Điện thoại:  (+84)82 966 3333
                    <br> Email: lienhe@rubysworld.vn
                    <br> Website: https://www.rubysworld.vn</p>
            </div>

            <div class="col col-md-6 col-12">
                <h1>Liên hệ với Ruby’s World</h1>
            </div>

        </div>
    </div>
</div>
<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <span class="logo" ><img src="<?php echo asset('public/images/Untitled-f.png');?>" alt=""></span>


                <h3 class="tencty">Cửa hàng điện thoại anh Tuấn </h3>
                <p class="gtct">Cung cấp điên thoại với giá rẻ nhất thị trường bao ngầu bao bền bao chất lượng</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Ung Tuấn</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
