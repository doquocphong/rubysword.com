<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng Ký</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/DangKy.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="form-container sign-in-container">
                <form action="#">
                    <h1>Đăng Ký</h1>
                    <input type="text" placeholder="Tên tài khoản" />
                    <input type="password" placeholder="Mật Khẩu" />
                    <input type="text" placeholder="Tên Khách hàng" />
                    <input type="date" name="bday" placeholder="Tên Khách hàng">

                    <select id="gender">
                        <option value="nam">Nam</option>
                        <option value="nu">Nữ</option>
                    </select>
                    <input type="text" placeholder="Địa chỉ" />
                    <input type="email" placeholder="Email" />
                    <input type="text" placeholder="số điện thoại" />
                    <button>Đăng Ký</button>
                </form>
            </div>
            <div class="overlay-container">
                <div class="overlay">
                    <img src="<?php echo asset('public/images/tt2.jpg');?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
