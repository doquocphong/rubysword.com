<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chi tiết sản phẩm</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Chitietsp.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid layer1">
    <div class="container">
        <div class="row layer11">

            <div class="col col-md-1 col-2">

            </div>

            <div class="col col-md-5 col-10">
                <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
            </div>
            <div class="col col-md-4 col-12">
                <h1>Kem trị mụn</h1>
                <h2>13.900.000đ</h2>
                <h3>16.500.000đ</h3>
                <p></p>
                <h4>Số lượng:</h4>
                <div class="quantity">

                    <input onclick="var result = document.getElementById('quantity'); var qty = result.value; if( !isNaN(qty) &amp; qty > 1 ) result.value--;return false;" type='button' value='-' />
                    <input id='quantity' min='1' name='quantity' type='text' value='1' />
                    <input onclick="var result = document.getElementById('quantity'); var qty = result.value; if( !isNaN(qty)) result.value++;return false;" type='button' value='+' />
                </div>
                <div class="button-mua">
                    <button type="button" class="btn btn-primary">Mua Hàng</button>
                </div>
            </div>

            <div class="col col-md-2 col-12 leftmain">
                <div class="row row1">
                    <p class="title">Sẽ có ngay tại nhà</p>
                    <p class="content">Nhận hàng sau 7 ngày</p>
                </div>
                <div class="row row1">

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </div>

                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p class="title">giao hàng miễn phí</p>
                        <p class="content">Đơn hàng trên 300k</p>
                    </div>


                </div>
                <hr class="hrngan">
                <div class="row row1">

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </div>

                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p class="title">Đổi trả miễn phí</p>
                        <p class="content">đổi trả miễn phí trong 30 ngày</p>
                    </div>
                </div>
                <hr class="hrngan">
                <div class="row row1">

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </div>

                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p class="title">Thanh toán </p>
                        <p class="content">Thanh toán khi nhận hàng</p>
                    </div>
                </div>
                <hr class="hrngan">
                <div class="row row1">

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </div>

                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p class="title">Hỗ trợ online</p>
                        <p class="content">0973599721</p>
                    </div>
                </div>
                <hr class="hrngan">
            </div>


        </div>
    </div>
</div>
<div class="container-fluid splienquan">
    <div class="container">
        <hr>
        <div class="row tsp">
            <h1>Sản phẩm liên quan</h1>
        </div>
        <div class="row sanphamtt">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail">
                    <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                    <div class="caption">
                        <h4>Kem trị mụn</h4>
                        <span class="price">
								<span class="sale-price">
									13.900.000đ
								</span>
								<span class="regular-price">
								16.500.000đ
								</span>
							</span>
                        <p>
                            <a href="#" class="btn btn-primary">Chi tiết</a>
                            <a href="#" class="btn btn-default">mua hàng</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail">
                    <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                    <div class="caption">
                        <h4>Kem trị mụn</h4>
                        <span class="price">
								<span class="sale-price">
									13.900.000đ
								</span>
								<span class="regular-price">
								16.500.000đ
								</span>
							</span>
                        <p>
                            <a href="#" class="btn btn-primary">Chi tiết</a>
                            <a href="#" class="btn btn-default">mua hàng</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail">
                    <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                    <div class="caption">
                        <h4>Kem trị mụn</h4>
                        <span class="price">
								<span class="sale-price">
									13.900.000đ
								</span>
								<span class="regular-price">
								16.500.000đ
								</span>
							</span>
                        <p>
                            <a href="#" class="btn btn-primary">Chi tiết</a>
                            <a href="#" class="btn btn-default">mua hàng</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail">
                    <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                    <div class="caption">
                        <h4>Kem trị mụn</h4>
                        <span class="price">
								<span class="sale-price">
									13.900.000đ
								</span>
								<span class="regular-price">
								16.500.000đ
								</span>
							</span>
                        <p>
                            <a href="#" class="btn btn-primary">Chi tiết</a>
                            <a href="#" class="btn btn-default">mua hàng</a>
                        </p>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Ruby's Word</h1>


                <h3 class="tencty">shop mỹ phẩm ruby's Word</h3>
                <p class="gtct">Cung cấp mỹ phẩm chất lượng</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
