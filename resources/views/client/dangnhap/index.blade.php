<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dang nhap</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Dangnhap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<h2>Chào mừng bạn đến với Ruby's Word</h2>
<div class="container" id="container">
    <div class="form-container sign-up-container">
        <form action="#">
            <h1>Tạo tài khoản</h1>
            <div class="social-container">
                <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <span>Bạn đã có tài khoản</span>
            <input type="text" placeholder="Name" />
            <input type="email" placeholder="Email" />
            <input type="password" placeholder="Password" />
            <button>Đăng ký</button>
        </form>
    </div>
    <div class="form-container sign-in-container">
        <form action="#">
            <h1>Đăng nhập</h1>
            <div class="social-container">
                <a href="#" class="social"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#" class="social"><span class="glyphicon glyphicon-user"></span></i></a>
                <a href="#" class="social"><span class="glyphicon glyphicon-heart"></span></i></a>
            </div>
            <span>Bạn đã có tài khoản</span>
            <input type="email" placeholder="Tên tài khoản" />
            <input type="password" placeholder="Mật Khẩu" />
            <button>Đăng Nhập</button>
        </form>
    </div>
    <div class="overlay-container">
        <div class="overlay">
            <div class="overlay-panel overlay-left">
                <h1>Chào Mừng Bạn</h1>
                <p>Bạn chưa có tài khoản hãy đăng ký với chúng tôi</p>
                <button class="ghost" id="signIn">Sign In</button>
            </div>
            <div class="overlay-panel overlay-right">
                <h1>Chào Bạn</h1>
                <p>Đăng ký tài khoản để mua hàng của shop</p>
                <button class="ghost" id="signUp"><a href="DangKy.html">Đăng Ký</a></button>
            </div>
        </div>
    </div>
</div>


</body>
</html>
