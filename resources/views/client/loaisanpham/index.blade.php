<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loai san pham</title>


    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/loaisanpham.css');?>">
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid loaisp">
    <div class="container">
        <div class="row">
            <div class="row tensp">

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <hr class="hrtensp">
                    <hr class="hrtensp">
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <h2>Kem trị mụn</h2>
                    <h4>sản phẩm luôn được cập nhật thường xuyên</h4>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <hr class="hrtensp">
                    <hr class="hrtensp">
                </div>

            </div>
            <div class="row sanphamtt">
                @foreach($product as $pr)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="thumbnail">
                            <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                            <div class="caption">
                                <h4>{{$pr->ProductName}}</h4>
                                <span class="price">
								<span class="sale-price">
									{{$pr->UnitPrice}}
								</span>
								<span class="regular-price">
								    @if($pr->SaleID!=null)
                                        @foreach($sale as $sa)
                                            @if($pr->SaleID == $sa->id)
                                                {{($pr->UnitPrice * (100-$sa->Discount))/100}}
                                            @endif
                                        @endforeach
                                    @endif
								</span>
							</span>
                                <p>
                                    <a href="Chitietsp.html" class="btn btn-primary">Chi tiết</a>
                                    <a href="#" class="btn btn-default">mua hàng</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row" style="text-align: right;">
                {{$product->links()}}
            </div>
        </div>
    </div>
</div>
<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Logo</h1>


                <h3 class="tencty">Cửa hàng điện thoại anh Tuấn </h3>
                <p class="gtct">Cung cấp điên thoại với giá rẻ nhất thị trường bao ngầu bao bền bao chất lượng</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
