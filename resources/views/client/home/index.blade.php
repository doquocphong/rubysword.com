<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang chủ</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Home.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid banner">
    <div class="container banner1">
        <div class="row banner1">
            <img src="<?php echo asset('public/images/bnmagic.jpg');?>" alt="">
        </div>
    </div>
</div>
<div class="container-fluid sanpham">
    <div class="container sanpham1">
        <div class="row sanpham1">

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img src="<?php echo asset('public/images/ic1.png');?>" class="oto" alt="">
                <h2>Miễn phí giao hàng </h2>
                <p>Tất cả các sản phẩm đều được vận chuyển miễn phí</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img src="<?php echo asset('public/images/ic2.png');?>" alt="">
                <h2>Đổi trả hàng </h2>
                <p>sản phẩm được phép đổi trả trong vòng 2 ngày</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img src="<?php echo asset('public/images//ic3.png');?>" alt="">
                <h2>Giao hàng nhận tiền </h2>
                <p>Thanh toán bằng hình thức trực tiếp </p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img src="<?php echo asset('public/images/ic4.png');?>" alt="">
                <h2>Đặt hàng online</h2>
                <p>0973599721</p>
            </div>
        </div>
        <div class="row tensp">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <hr class="hrtensp">
                <hr class="hrtensp">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h2>KEM TRỊ MỤN</h2>
                <h4>sản phẩm luôn được cập nhật thường xuyên</h4>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <hr class="hrtensp">
                <hr class="hrtensp">
            </div>

        </div>
        <div class="row sanphamtt">
            @foreach($product_1 as $pr1)
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail">
                    <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                    <div class="caption">
                        <h4>{{$pr1->ProductName}}</h4>
                        <span class="price">
								<span class="sale-price">
									{{$pr1->UnitPrice}}
								</span>
								<span class="regular-price">
								    @if($pr1->SaleID!=null)
                                        @foreach($sale as $sa)
                                            @if($pr1->SaleID == $sa->id)
                                                {{($pr1->UnitPrice * (100-$sa->Discount))/100}}
                                            @endif
                                        @endforeach
                                    @endif
								</span>
							</span>
                        <p>
                            <a href="Chitietsp.html" class="btn btn-primary">Chi tiết</a>
                            <a href="#" class="btn btn-default">mua hàng</a>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row tensp">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <hr class="hrtensp">
                <hr class="hrtensp">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h2>Mặt Nạ</h2>
                <h4>sản phẩm luôn được cập nhật thường xuyên</h4>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <hr class="hrtensp">
                <hr class="hrtensp">
            </div>

        </div>
        <div class="row sanphamtt">

            @foreach($product_2 as $pr2)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="thumbnail">
                        <img src="<?php echo asset('public/images/magicskin.jpg');?>" alt="">
                        <div class="caption">
                            <h4>{{$pr2->ProductName}}</h4>
                            <span class="price">
								<span class="sale-price">
									{{$pr2->UnitPrice}}
								</span>
								<span class="regular-price">
								    @if($pr2->SaleID!=null)
                                        @foreach($sale as $sa)
                                            @if($pr2->SaleID == $sa->id)
                                                {{($pr2->UnitPrice * (100-$sa->Discount))/100}}
                                            @endif
                                        @endforeach
                                    @endif
								</span>
							</span>
                            <p>
                                <a href="Chitietsp.html" class="btn btn-primary">Chi tiết</a>
                                <a href="#" class="btn btn-default">mua hàng</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach


    </div>
</div>
<div class="container-fluid tintuc1">
    <div class="container tintuc">
        <div class="row tt">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row tensp">

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <hr class="hrtensp">
                        <hr class="hrtensp">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <h2>Tin Tức</h2>
                        <h4>sản phẩm luôn được cập nhật thường xuyên</h4>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <hr class="hrtensp">
                        <hr class="hrtensp">
                    </div>

                </div>
            </div>
            <div class="row cttt">
                <div class="col col-md-4 col-12">
                    <div class="thumbnail">
                        <img src="<?php echo asset('public/images/tt1.png');?>" alt="">
                        <div class="caption">
                            <h4>Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</h4>
                            <p class="time">ngày:05/09/2017</p>
                            <p class="content">
                                Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-12">
                    <div class="thumbnail">
                        <img src="<?php echo asset('public/images/tt1.png');?>" alt="">
                        <div class="caption">
                            <h4>Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</h4>
                            <p class="time">ngày:05/09/2017</p>
                            <p class="content">
                                Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-12">
                    <div class="thumbnail">
                        <img src="<?php echo asset('public/images/tt1.png');?>" alt="">
                        <div class="caption">
                            <h4>Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</h4>
                            <p class="time">ngày:05/09/2017</p>
                            <p class="content">
                                Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố...
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Ruby's word</h1>


                <h3 class="tencty">Shop mỹ phẩm ruby word</h3>
                <p class="gtct">Cung cấp mỹ phẩm đủ tiêu chuẩn thị trường</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
