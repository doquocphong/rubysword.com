<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Giohang</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Giohang.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid layer1">
    <div class="container">
        <div class="row rowtitle">
            <h1>Giỏ hàng</h1>
        </div>
        <div class="row cart">

            <div class="page-cart">
                <div class="container">
                    <div class="list-cart">
                        <div class="title item-cart">
                            <div class="name">
                                <p>Tên sản phẩm</p>
                            </div>
                            <div class="price">
                                <p>Giá</p>
                            </div>
                            <div class="h-number">
                                <p>Số lượng</p>
                            </div>
                            <div class="delete">
                                <p></p>
                            </div>
                        </div>
                        <div class="item-cart" id="item-cart-75,73">
                            <div class="name">
                                <a href=""><img src="<?php echo asset('public/images/Unico 1-5.jpg');?>" alt="Tên sp "></a>
                                <div class="infor">
                                    <h3><a href="">Tên sản phẩm </a></h3>

                                    <p class="sku" style="font-size: 12px;color: #b20000;">mô tả sp</p>

                                </div>
                            </div>
                            <div class="price">
                                <h3><span class="payment-item">931.500</span> <sup>đ</sup></h3>
                                <input type="hidden" value="931500">
                                <b>1.500</b>
                                <span class="label">( -38% )</span>
                            </div>
                            <div class="add-quantity">
                                <input onclick="var result = document.getElementById('quantity'); var qty = result.value; if( !isNaN(qty) &amp; qty > 1 ) result.value--;return false;" type='button' value='-' />
                                <input id='quantity' min='1' name='quantity' type='text' value='1' />
                                <input onclick="var result = document.getElementById('quantity'); var qty = result.value; if( !isNaN(qty)) result.value++;return false;" type='button' value='+' />
                                <span class="glyphicon glyphicon-refresh"></span>
                            </div>
                            <a href="javascript:void(0);" class="js-remove-item" data-product="163" data-variant="75,73">✖</a>
                        </div>
                    </div>

                    <div class="content-cart">
                        <!-- <h3>Giỏ hàng của tôi</h3> -->
                        <div class="infor-order">
                            <h3>Thông tin đơn hàng</h3>
                            <div class="infor">
                                <p>Tạm tính</p>
                                <p><span id="total_money_1">931.500</span> <sup>đ</sup></p>
                            </div>
                            <div class="infor total">
                                <p>Tổng tiền</p>
                                <p><span id="total_money_2">931.500</span> <sup>đ</sup></p>
                            </div>
                            <a href="/checkout">Tiến hành thanh toán</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Ruby's word</h1>


                <h3 class="tencty">Tập đoàn mỹ phẩm ruby word</h3>
                <p class="gtct">Công ty sản xuất và phân phối các sản phẩm làm đẹp và thực phẩm chức năng được bộ y tế cấp phép lưu hành</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">về chúng tôi</a> <br><br>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">Magicskin</a> <br><br>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">Magicmom </a> <br><br>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">Tin tức</a> <br><br>
                <span class="glyphicon glyphicon-play"></span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
