<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tin Tuc</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Tintuc.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>

</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid tintuc">
    <div class="container tintuc1">
        <h1>Tin Tức</h1>
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
        <div class="row news">
            <div class="col col-md-3 col-12">
                <a href="Chitiettintuc.html"> <img src="<?php echo asset('public/images/tt1.png');?>" alt=""></a>
            </div>
            <div class="col col-md-9 col-12">
                <a href="Chitiettintuc.html"><p class="title">Iran tuyên bố đã đuổi máy bay do thám U2 của Mỹ</p></a>
                <p>Chúng ta vẫn biết rằng, làm việc với một đoạn văn bản dễ đọc và rõ nghĩa dễ gây rối trí và cản trở việc tập trung vào yếu tố trình bày văn bản. Lorem Ipsum có ưu điểm hơn so với đoạn văn bản chỉ gồm nội dung kiểu “Nội dung, nội dung, nội dung” là nó khiến văn bản giống thật hơn, bình thường...</p>
                <a href="Chitietsp.html" class="btn btn-dark">Chi tiết</a>
            </div>
        </div>
        <hr class="ngan">
    </div>
</div>
<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Logo</h1>


                <h3 class="tencty">Cửa hàng điện thoại anh Tuấn </h3>
                <p class="gtct">Cung cấp điên thoại với giá rẻ nhất thị trường bao ngầu bao bền bao chất lượng</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="<?php echo asset('public/images/footerFanPage_03.png');?>" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="<?php echo asset('public/images/footerMap_05.png');?>" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Ung Tuấn</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
