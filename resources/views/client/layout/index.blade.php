<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang chủ</title>
    <script type="text/javascript" src="vendor/bootstrap.js"></script>
    <link rel="stylesheet" href="vendor/bootstrap.css">
    <link rel="stylesheet" href="Home.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <script src="js/jQuery3.min.lib.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/Home.js"></script>
    <style>
        @yield('css')
    </style>
</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="Giohang.html" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="Dangnhap.html" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="DangKy.html" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="Home.html">Trang chủ</a></li>
                <li><a href="GioiThieu.html">giới thiệu</a></li>
                <li><a href="loaisp.html">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="TinTuc.html">tin tức</a></li>
                <li><a href="">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid banner">
    <div class="container banner1">
        <div class="row banner1">
            <img src="images/bnmagic.jpg" alt="">
        </div>
    </div>
</div>

<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Ruby's word</h1>


                <h3 class="tencty">Shop mỹ phẩm ruby word</h3>
                <p class="gtct">Cung cấp mỹ phẩm đủ tiêu chuẩn thị trường</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
                <img src="images/footerFanPage_03.png" class="fanpage">
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>
                <img src="images/footerMap_05.png" class="maphome">
            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
