<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Header and footer</title>

    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap.js');?>"></script>
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/Gioithieu.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick.css');?>">
    <link rel="stylesheet" href="<?php echo asset('public/css/slick-theme.css"');?>">
    <script src="<?php echo asset('public/js/jQuery3.min.lib.js');?>"></script>
    <script src="<?php echo asset('public/js/slick.min.js');?>"></script>
    <script src="<?php echo asset('public/js/Home.js');?>"></script>

</head>
<body>
<div class="container-fluid header1">
    <div class="container header">
        <div class="row ly1">
            <div class="col col-md-3 col-12">
                <div class="row logo">
                    <h1 class="logo">Ruby's Word</h1>
                </div>
            </div>
            <div class="col col-md-5 col-12">
                <div class="row form-group">
                    <input type="text" class="form-control" placeholder="Sản phẩm muốn tìm">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </div>

            <div class="col col-md-2 col-12">
                <div class="row giohang">
                    <a href="<?php echo url('/giohang')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-cart "></i>
                        Giỏ hàng
                    </a>
                </div>
            </div>
            <div class="col col-md-2 col-12">
                <div class="row dangnhap">
                    <a href="<?php echo url('/dangnhap')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng nhập
                    </a>
                    <a href="<?php echo url('/dangky')?>" class="shop_cart" title="Giỏ hàng">
                        <i class="fa fa-shopping-heart "></i>
                        Đăng ký
                    </a>
                </div>
            </div>
        </div>
        <div class="row lymenu">
            <hr class="ngan">
            <ul>
                <li><a href="<?php echo url('/')?>">Trang chủ</a></li>
                <li><a href="<?php echo url('/gioithieu')?>">giới thiệu</a></li>
                <li><a href="<?php echo url('/loaisanpham')?>">sản phẩm</a></li>
                <li><a href="">dịch vụ</a></li>
                <li><a href="<?php echo url('/tintuc')?>">tin tức</a></li>
                <li><a href="<?php echo url('/lienhe')?>">liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid gt">
    <div class="container gioithieu">
        <div class="row">
            <h1 class="center">GIỚI THIỆU</h1>
            <p>Công ty TNHH Tập Đoàn Ruby’s World là công ty chuyên sản xuất, gia công và phân phối mỹ phẩm. Với 2 thương hiệu mỹ phẩm cao cấp là Magic Skin & Magic Mom có được vị trí khá cao trên thị trường nhờ dòng sản phẩm chăm sóc cơ thể, chất lượng tốt, mẫu mã đẹp, giá thành hợp lý, phù hợp với cơ địa của người Châu Á nói chung và người Việt Nam nói riêng.
                Với tuổi đời trên 5 năm, Công ty TNHH Tập Đoàn Ruby’s World do CEO Đào Minh Châu lãnh đạo đã khẳng định được vị trí của mình trong ngành mỹ phẩm trong và ngoài nước.
            </p>
            <img src="<?php echo asset('public/images/Chau.jpg');?>" alt="">
            <br>
            <h6 class="center">Đào Minh Châu – Chủ tịch HĐQT kiêm Tổng Giám đốc Công ty TNHH Tập Đoàn Ruby’s World

                Ruby’s World sở hữu nhà máy đạt chuẩn sản xuất mỹ phẩm ở Việt Nam, được Sở Y tế TP Hà Nội cấp giấy Chứng nhận đủ điều kiện sản xuất mỹ phẩm với diện tích gần 2000m2 tại khu công nghiệp Thạch Thất, huyện Quốc Oai. Nhà máy theo chuẩn ISO 9001:2015 được đầu tư bài bản, đồng bộ về nhà xưởng, trang thiết bị, mọi quá trình sản xuất và quản lý thực hiện theo mô hình  tự động.</h6>
            <img src="<?php echo asset('public/images/cong.jpg');?>" alt="">
            <br>
            <h6 class="center">Nhà xưởng sản xuất của Công ty đặt tại khu công nghiệp Thạch Thất <br>– Huyện Quốc Oai – Hà Nội.</h6>
            <h1 class="center">CHÍNH SÁCH CHẤT LƯỢNG SẢN PHẨM</h1>
            <p>Ban lãnh đạo và toàn thể cán bộ, nhân viên Ruby’s World cam kết thực hiện các chính sách nhằm không ngừng nâng cao chất lượng sản phẩm của công ty. Đảm bảo cung cấp cho khách hàng các sản phẩm đạt tiêu chuẩn chất lượng an toàn.
                <br>– Nguyên liệu đầu vào tại Ruby’s World đều được nhập khẩu từ những quốc gia có nền công nghiệp hóa mỹ phẩm phát triển trên thế giới. Các yếu tố thiên nhiên, thân thiện, an toàn với môi trường và người sử dụng luôn được ưu tiên hàng đầu.</p>
            <p>CEO Đào Minh Châu giới thiệu về công nghệ sản xuất với các nhà phân phối và đại lý.</p>
            <p>
                – Khâu kiểm tra nguyên liệu đầu vào luôn được chú trọng và được thực hiện bởi những chuyên gia hàng đầu.
                <br>– Nằm tại khu công nghiệp Thạch Thất, huyện Quốc Oai, nhà máy sản xuất của công ty có diện tích gần 2000m2 được chia thành các khu chuyên biệt: Phòng nghiên cứu, phòng pha chế, khu sản xuất, nhà ăn tập thể , khu nhà ở và sinh hoạt cho công nhân… Trang thiết bị dây chuyền hiện đại, được nhập khẩu đồng bộ, cùng quy trình vận hành khép kín chuẩn hóa quốc tế, Ruby’s World là một trong những nhà máy sản xuất gia công mỹ phẩm lớn nhất Việt Nam hiện nay.
            </p>
            <br>
            <img src="<?php echo asset('public/images/nguyenlieu.jpg');?>" alt="">
            <h6 class="center">Công nhân sản xuất phải đảm bảo quy chuẩn bảo hộ nghiêm ngặt khi làm việc.
            </h6>

            <p>– Hệ thống máy móc được chú trọng đầu tư hiện đại, đảm bảo những công nghệ mới nhất trong ngành công nghiệp hóa mỹ phẩm: máy đồng hóa, hệ thống máy nhũ hóa, máy chiết rót,… được kiểm tra, bảo dưỡng định kỳ nhằm đảm bảo mọi hoạt động trơn tru và sản lượng đầu ra luôn kịp tiến độ.
            </p>
            <img src="<?php echo asset('public/images/nguyenlieu2.jpg');?>" alt="">
            <h6 class="center">Hệ thống máy móc hiện đại theo công nghệ sản xuất mới nhất được chú ý đầu tư</h6>
            <p>– Với hơn 100 công nhân lành nghề cùng các chuyên gia nhiều năm kinh nghiệm liên tục được thi kiểm tra tay nghề và đào tạo sáu tháng một lần tạo ra một môi trường làm việc chuyên nghiệp, chất lượng nhân sự tuyệt vời, đảm bảo chất lượng sản phẩm luôn đạt chuẩn và an toàn.</p>
            <img src="<?php echo asset('public/images/nguyenlieu3.jpg');?>" alt="">
            <h6 class="center">Cẩn thận, chỉn chu với từng giai đoạn sản xuất của đội ngũ công nhân lành nghề đầy tâm huyết</h6>
            <p>– Mọi sản phẩm làm ra đều phải trải qua quy trình thử nghiệm, kiểm tra gắt gao trước khi được đưa vào ứng dụng đại trà.
                <br>– Là một trong số ít những đơn vị sở hữu nhà máy đạt chuẩn sản xuất mỹ phẩm ở Việt Nam, được Sở Y tế TP Hà Nội cấp Giấy chứng nhận đủ điều kiện sản xuất mỹ phẩm. Nhà máy được xây dựng theo chuẩn ISO 9001:2015 của Ruby’s World có thể cung ứng ra thị trường gần một triệu sản phẩm hoàn thiện mỗi tháng.</p>

            <h3>Tập Đoàn Ruby’s World tự hào là doanh nghiệp đạt nhiều danh hiệu và cup vàng do các tổ chức uy tín và người tiêu dùng bình chọn:</h3>
            <p>– Giấy chứng nhận ISO 9001:2015
                <br>– Doanh nghiệp vàng thế kỷ 21
                <br>– Thương hiệu chăm sóc sắc đẹp đẳng cấp Magic Skin/ MagicMom
                <br>– Top 100 thương hiệu sản phẩm nổi tiếng tại Việt Nam năm 2016 Người tiêu dùng bình chọn
                <br>– Thương hiệu xuất sắc 3 miền: MagiSkin/ MagicMom</p>
            <h3>SỨ MỆNH</h3>
            <p>Đúng như slogan của thương hiệu: MagicSkin <br>– Phép thuật thay đổi làn da và MagicMom <br>– Gìn giữ nét thanh xuân cho mẹ bầu, gần 40 sản phẩm thuộc mạng lưới Ruby’s World Group đều hướng tới yếu tố an toàn, thân thiện, sự chăm sóc và lột xác kỳ diệu cho người dùng.
                Với triết ký kinh doanh: ” Mang đến sự hài lòng cho khách hàng bằng hạnh phúc của nhân viên.”  Ruby’s World luôn nỗ lực xây dựng duy trì niềm tin của cộng đồng sản xuất mỹ phẩm của người Việt. Ở đó người công nhân hăng say sản xuất, người nhân viên hăng hái làm việc và người tiêu dùng hài lòng về sản phẩm và dịch vụ thông qua trải nghiệm hạnh phúc.</p>
            <h3>TẦM NHÌN</h3>
            <p>Bằng khát vọng tuổi trẻ và những cố gắng không ngừng nghỉ của CEO Đào Minh Châu cùng đội ngũ nhân lực đầy nhiệt huyết và đam mê, Ruby’s World phấn đấu trở thành công ty mỹ phẩm hiện đại hàng đầu Đông Nam Á. Đưa những sản phẩm chất lượng về chăm sóc sức khỏe, sắc đẹp tới hàng triệu người dân Việt và phủ sóng thương hiệu vươn ra Thế giới.</p>
        </div>
    </div>
</div>

<div class="container-fluid footer1">
    <div class="container footer">
        <div class="row footer11">

            <div class="col col-md-4 col-12">


                <h1 class="logo" style="color: aqua;">Ruby's word</h1>


                <h3 class="tencty">Shop mỹ phẩm Ruby's word</h3>
                <p class="gtct">cung cấp mỹ phẩm chất lượng nhất thị trường</p>
                <b><i class="fa fa-facebook-square face" aria-hidden="true"></i></b>
                <i class="fa fa-instagram insta" aria-hidden="true"></i>
                <i class="fa fa-skype sky" aria-hidden="true"></i>
                <i class="fa fa-youtube-square" aria-hidden="true"></i>


            </div>

            <div class="col col-md-2 col-12">
                <h3 class="menu">MENU</h3>
                </span> <a href="#" class="a1">Trang chủ</a> <br><br>
                </span> <a href="#" class="a1">Giới thiệu</a> <br><br>
                </span> <a href="#" class="a1">Sản Phẩm</a> <br><br>
                </span> <a href="#" class="a1">Tin tức</a> <br><br>
                </span> <a href="#" class="a1">Liên Hệ</a> <br><br>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">FANPAGE</h3>
            </div>

            <div class="col col-md-3 col-12">
                <h3 class="menu">MAP</h3>

            </div>

        </div>
        <div class="copyright">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="hrcopy">
                <h4 class="copyright1">Design by Trịnh Tất</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>
