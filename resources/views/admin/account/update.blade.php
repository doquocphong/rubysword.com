@extends('admin.layout.index')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Form them</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Form update accountr</h3>
            <br>
            <small>{{$account->full_name}}</small>
        </div>
        @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors -> all() as $err)
                    {{$err}}<br>
                @endforeach
            </div>
        @endif

        @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
    @endif
    <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="<?php echo url('admin/account/update')?>/{{$account->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Account ID</label>
                    <input type="text" class="form-control" id="inputID"
                           placeholder="Enter account ID" name="ID" disabled
                           value="{{$account->id}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Account Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1"
                           placeholder="Enter email" name="email"
                           value="{{$account->email}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="text" class="form-control" id="exampleInputPassword1"
                           placeholder="Password" name="password"
                           value="{{$account->password}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Note</label>
                    <input type="text" class="form-control" id="exampleInputFullname1"
                           placeholder="Enter fullname" name="fullname"
                           value="{{$account->Note}}">
                </div>
                <div class="form-group">
                    <label style="width: 100px">Role</label>
                    <label class="radio-inline" style="width: 100px">
                        <input name="role" value="0"
                               @if($account->Role ==0)
                               {{"checked"}}
                               @endif type="radio">Thường
                    </label>
                    <label class="radio-inline" style="width: 100px">
                        <input name="role" value="1"
                               @if($account->Role ==1)
                               {{"checked"}}
                               @endif type="radio">Admin
                    </label>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#changepassword").change(function () {
                if ($(this).is(":checked")) {
                    $(".password").removeAttr('disabled');
                } else {
                    $(".password").attr('disabled', '');
                }
            });
        });
    </script>
@endsection
