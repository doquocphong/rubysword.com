@extends('admin.layout.index')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Form add product</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="card-body pad">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form add product</h3>
                        <br>
                        <small>{{$products->name}}</small>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors -> all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <form role="form" method="POST" action="<?php echo url('admin/product/update')?>/{{$products->id}}"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="product_name">ProductID</label>
                                <input type="text" class="form-control" placeholder="input id_product"
                                       name="id_product" id="id_product" disabled value="{{$products->id}}">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Name</label>
                                <input type="text" class="form-control" placeholder="Input name of product"
                                       name="product_name" id="product_name" value="{{$products->ProductName}}">
                            </div>
                            <div class="form-group">
                                <label for="id_type_product">Type_product</label><br>
                                <select class="form-group custom-select" name="id_type_product" id="id_type_product">
                                    @foreach($type as $tp)
                                        <option
                                            @if($type_products->id==$tp->id)
                                            {{"selected"}}
                                            @endif

                                            value="{{$tp->id}}">{{$tp->TypeProductName}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Unit_price</label>
                                <input type="text" class="form-control" placeholder="Input unit_price"
                                       name="unit_price" value="{{$products->UnitPrice}}">
                            </div>
                            <div class="form-group">
                                <label for="id_type_product">Sale</label><br>
                                <select class="form-group custom-select" name="id_sale" id="id_sale">
                                    @foreach($sale as $sa)
                                        <option
                                            @if($sale_p->id==$sa->id)
                                            {{"selected"}}
                                            @endif

                                            value="{{$sa->id}}">{{$sa->Discount}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_price">Image_URL</label>
                                <input type="text" class="form-control" placeholder=""
                                       name="url" id="url" value="{{$products->ImageURL}}">
                            </div>
                            <div class="form-group">
                                <label for="unit">Note</label>
                                <input type="text" class="form-control" placeholder="Input note" name="note"
                                       id="note" value="{{$products->Note}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <div class="mb-3">
                                <textarea class="textarea" placeholder="Place some text here"
                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                          name="description" value="{{$products->Description}}"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">Detail</label>
                                <div class="mb-3">
                                <textarea class="textarea" placeholder="Place some text here"
                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                          name="detail" value="{{$products->Detail}}"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

@endsection
