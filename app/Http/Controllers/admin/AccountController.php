<?php

namespace App\Http\Controllers\admin;

use App\account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function index()
    {
        $account = account::all();
        return view('admin.account.display', ['account' => $account]);
    }

    public function create()
    {
        return view('admin.account.add');
    }

    public function store(Request $request)
    {
//        $this->validate($request,
//            [
//                'email' => 'required|unique:users,email|min:13|max:100|email',
//                'password' => 'required|min:8|max:16',
//                'fullname' => 'required|min:5|max:50',
//                'confirmpassword' => 'required|same:password'
//            ],
//            [
//                'email.required' => "Bạn chưa nhập email",
//                'email.unique' => 'Email đã tồn tại',
//                'email.min' => 'email phải có độ dài từ 13 đến 100 ký tự',
//                'email.max' => 'email phải có độ dài từ 13 đến 100 ký tự',
//                'email.email' => 'Bạn chưa nhập đúng định dạng email',
//
//                'password.required' => "Bạn chưa nhập mật khẩu",
//                'password.min' => "Mật khẩu phải có độ dài từ 8 đến 16 ký tự",
//                'passowrd.max' => "Mật khẩu phải có độ dài từ 8 đến 16 ký tự",
//
//                'fullname.required' => 'Bạn chưa  nhập tên',
//                'fullname.min' => "Tên phải có độ dài từ 5 đến 50 ký tự",
//                'fullname.max' => "Tên phải có độ dài từ 5 đến 50 ký tự",
//
//                'confirmpassword.same' => "Mật khẩu nhập lại không khớp"
//            ]);

        $account = new account();
        $account->id = $request->ID;
        $account->email = $request->email;
        $account->password = $request->password;
        $account->Note = $request->fullname;
        $account->Role = $request->role;

        $account->save();

        return redirect('admin/account/add')->with('thongbao', 'Thêm thành công');
    }

    public function edit($id)
    {
        $account = account::find($id);
        return view('admin.account.update', ['account' => $account]);
    }

    public function update(Request $request, $id)
    {
//        $this->validate($request,
//            [
//                'fullname' => 'required|min:5|max:50',
//            ],
//            [
//                'fullname.required' => 'Bạn chưa  nhập tên',
//                'fullname.min' => "Tên phải có độ dài từ 5 đến 50 ký tự",
//                'fullname.max' => "Tên phải có độ dài từ 5 đến 50 ký tự",
//            ]);

        $account = account::find($id);
        $account->email = $request->email;
        $account->password = $request->password;
        $account->role = $request->role;
        $account->Note=$request->fullname;

//        if ($request->changepassword == "on") {
//            $this->validate($request,
//                [
//                    'password' => 'required|min:8|max:16',
//                    'confirmpassword' => 'required|same:password'
//                ],
//                [
//                    'password.required' => "Bạn chưa nhập mật khẩu",
//                    'password.min' => "Mật khẩu phải có độ dài từ 8 đến 16 ký tự",
//                    'passowrd.max' => "Mật khẩu phải có độ dài từ 8 đến 16 ký tự",
//
//                    'confirmpassword.same' => "Mật khẩu nhập lại không khớp"
//                ]);
//            $user->password = bcrypt($request->password);
//        }
        $account->save();

        return redirect('admin/account/update/' . $id)->with('thongbao', 'Sửa thành công');
    }
    public function getLoginAdmin()
    {
        return view('admin.login');
    }

    public function postLoginAdmin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => "Bạn chưa nhập email",
            'password.required' => "Bạn chưa nhập mật khẩu",
        ]);
        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password))) {
            return redirect('admin/layout/index');
        } else {
            return redirect('admin/login')->with('thongbao', 'Dang nhap khong thanh cong');
        }
    }

    public function destroy($id)
    {
        $user = account::find($id);
        $user->delete();
        return redirect('admin/account/display')->with('thongbao', 'Xóa thành công');
    }

    public function getLogoutAdmin()
    {
        Auth::logout();
        return redirect('admin/login');
    }
}
