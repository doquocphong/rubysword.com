<?php

namespace App\Http\Controllers\admin;

use App\order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $order = order::all();
        return view('admin.order.display', ['order' => $order]);
    }
}
