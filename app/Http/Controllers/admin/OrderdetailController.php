<?php

namespace App\Http\Controllers\admin;

use App\orderdetail;
use Illuminate\Http\Request;

class OrderdetailController extends Controller
{
    public function index()
    {
        $orderdetail = orderdetail::all();
        return view('admin.orderdetail.display', ['orderdetail' => $orderdetail]);
    }
}
