<?php

namespace App\Http\Controllers\admin;

use App\typeproduct;
use Illuminate\Http\Request;

class TypeproductController extends Controller
{
    public function index()
    {
        $typeproduct = typeproduct::all();
        return view('admin.typeproduct.display', ['typeproduct' => $typeproduct]);
    }
}
