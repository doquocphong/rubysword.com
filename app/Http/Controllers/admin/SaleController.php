<?php

namespace App\Http\Controllers\admin;

use App\sale;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index()
    {
        $sale = sale::all();
        return view('admin.sale.display', ['sale' => $sale]);
    }
}
