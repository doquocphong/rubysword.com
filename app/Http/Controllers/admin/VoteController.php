<?php

namespace App\Http\Controllers\admin;

use App\vote;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    public function index()
    {
        $vote = vote::all();
        return view('admin.vote.display', ['vote' => $vote]);
    }
}
