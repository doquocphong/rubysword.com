<?php

namespace App\Http\Controllers\admin;

use App\customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $customer = customer::all();
        return view('admin.customer.display', ['customer' => $customer]);
    }
}
