<?php

namespace App\Http\Controllers\admin;


use App\sale;
use App\typeproduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\product;

class ProductController extends Controller
{
    public function index()
    {
        $products = product::all();
        return view('admin.product.display', ['products' => $products]);
    }

    public function create()
    {
        $type_product = typeproduct::all();
        $sale = sale::all();
        return view('admin.product.add',['type_products' => $type_product,'sale' => $sale]);
    }

    public function store(Request $request)
    {
//        $this->validate($request, [
//            'name' => 'required|unique:products,name',
//            'id_type_product' => 'required',
//            'unit_price' => 'required',
//            'promotion_price' => 'required',
//            'image' => 'required',
//            'unit' => 'required',
//            'description' => 'required'
//        ], [
//            'name.required' => 'Bạn chưa nhập tên sản phẩm',
//            'description.required' => 'Bạn chưa nhập mô tả sản phẩm'
//        ]);

        $product=new product();
        $product->id = $request->id_product;
        $product->ProductName = $request->product_name;
        $product->TypeProductID = $request->id_type_product;
        $product->UnitPrice = $request->unit_price;
        $product->SaleID = $request->id_sale;
        $product->ImageURL = $request->url;
        $product->Note = $request->note;
        $product->Detail = $request->detail;
        $product->Description = $request->description;

//        if ($request->hasFile('image')) {
//            $file = $request->file('image');
//
//            $extension = $file->getClientOriginalExtension();
//            if ($extension != "jpg" && $extension != "png" && $extension != "jpeg") {
//                return redirect('admin/product/add')->with('thongbao', 'Phần mở rộng không hợp lệ (Đuôi chỉ có thể là png,jpg,jpeg)');
//            }
//
//            $name = $file->getClientOriginalName();
//            $image = Str::random(4) . "_" . $name;
//            while (file_exists(public_path('upload/product/') . $image)) {
//                $image = Str::random(4) . "_" . $name;
//            }
//            $file->move(public_path('upload/product'), $image);
//            $product->image = $image;
//        } else {
//            $product->image = "";
//        }
        $product->save();

//        $files = $request->file('img');
//        foreach ($files as $file) {
//            $imagee = new Image();
//            $imagee->name = $product->name;
//            $imagee->id_product = $product->id;
//
//            if ($request->hasFile('img')) {
//                $extension = $file->getClientOriginalExtension();
//                if ($extension != "jpg" && $extension != "png" && $extension != "jpeg") {
//                    return redirect('admin/image_new/add')->with('thongbao', 'Phần mở rộng không hợp lệ (Đuôi chỉ có thể là png,jpg,jpeg)');
//                }
//
//                $name = $file->getClientOriginalName();
//                $image = Str::random(4) . "_" . $name;
//                while (file_exists(public_path('upload/image_product/') . $image)) {
//                    $image = Str::random(4) . "_" . $name;
//                }
//                $file->move(public_path('upload/image_product'), $image);
//                $imagee->image = $image;
//            }else {
//                $imagee->image = "";
//            }
//            $imagee->save();
//        }

        return redirect('admin/product/add')->with('thongbao', 'Thêm thành công');
    }

    public function edit($id)
    {
        $product = product::find($id);
        $type_product = typeproduct::find($product->TypeProductID);
        $type=typeproduct::all();
        $sale=sale::all();
        $sale_p=sale::find($product->SaleID);
        return view('admin.product.update', ['products' => $product, 'type_products' => $type_product,'type' => $type
        ,'sale'=>$sale,'sale_p'=>$sale_p]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $this->validate($request,
            [

            ],[

            ]);

        $product->ProductName = $request->product_name;
        $product->TypeProductID = $request->id_type_product;
        $product->UnitPrice = $request->unit_price;
        $product->SaleID = $request->id_sale;
        $product->ImageURL = $request->url;
        $product->Note = $request->note;
        $product->Detail = $request->detail;
        $product->Description = $request->description;

        $product->save();
        return redirect('admin/product/update/' . $id)->with('thongbao', 'Sửa thành công');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('admin/product/display')->with('thongbao', 'Xóa thành công');
    }

    public function search(Request $request){
        $products=Product::where('name','like','%'.$request->key.'%')->paginate(15);

        return view('admin.products.display',['products'=>$products]);
    }
}
