<?php

namespace App\Http\Controllers\admin;

use App\imageproduct;
use Illuminate\Http\Request;

class ImageproductController extends Controller
{
    public function index()
    {
        $images = imageproduct::all();
        return view('admin.imageproduct.index', ['images' => $images]);
    }
}
