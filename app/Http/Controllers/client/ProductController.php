<?php

namespace App\Http\Controllers\client;

use App\product;
use App\sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
        //$product=product::all();
        $product=DB::table('products')->paginate(8);
        $sale=sale::all();

        return view('client.loaisanpham.index',['product'=>$product,'sale'=>$sale]);
    }
}
