<?php

namespace App\Http\Controllers\client;

use App\product;
use App\sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){

        $product=product::all();
        $product_1=product::where('TypeProductID',1)->paginate(4);
        $product_2=product::where('TypeProductID',2)->paginate(4);
        $sale=sale::all();



        return view('client.home.index',['product'=>$product,'product_1'=>$product_1,'product_2'=>$product_2,
            'sale'=>$sale]);
    }
}
