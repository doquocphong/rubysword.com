<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//client--------------------------------------------------------------------------------------------------------
Route::get('admin/layout', function () {
    return view('admin.layout.index');
});

//Route::get('/', function () {
//    return view('client.home.index');
//});
Route::get('/','client\HomeController@index');
Route::get('/loaisanpham','client\ProductController@index');

Route::get('/chitietsanpham', function () {
    return view('client.chitietsanpham.index');
});

Route::get('/chitiettintuc', function () {
    return view('client.chitiettintuc.index');
});

Route::get('/dangky', function () {
    return view('client.dangky.index');
});

Route::get('/dangnhap', function () {
    return view('client.dangnhap.index');
});

Route::get('/giohang', function () {
    return view('client.giohang.index');
});

Route::get('/gioithieu', function () {
    return view('client.gioithieu.index');
});

Route::get('/lienhe', function () {
    return view('client.lienhe.index');
});

//Route::get('/loaisanpham', function () {
//    return view('client.product.index');
//});

Route::get('/tintuc', function () {
    return view('client.tintuc.index');
});


//admin-------------------------------------------------------------------------------------------------------------
Route::get('admin/login','admin\UserController@getLoginAdmin');
Route::post('admin/login','admin\UserController@postLoginAdmin');
Route::get('admin/logout','admin\UserController@getLogoutAdmin');

Route::get('admin/orderdetail/display','admin\OrderdetailController@index');

Route::group(['prefix'=>'admin'],function (){
    //customer
    Route::group(['prefix'=>'customer'],function (){
        Route::get('display','admin\CustomerController@index');

        Route::get('add','admin\CustomerController@create');
        Route::post('add','admin\customerController@store');

        Route::get('update/{id}','admin\CustomerController@edit');
        Route::post('update/{id}','admin\CustomerController@update');
    }) ;

    //account
    Route::group(['prefix'=>'account'],function (){
        Route::get('display','admin\UserController@index');

        Route::get('add','admin\UserController@create');
        Route::post('add','admin\UserController@store');

        Route::get('update/{id}','admin\UserController@edit');
        Route::post('update/{id}','admin\UserController@update');

        Route::get('delete/{id}','admin\UserController@destroy');

        Route::get('search','admin\UserController@search');
    });

    //user
    Route::group(['prefix'=>'user'],function (){
        Route::get('display','admin\UserController@index');

        Route::get('add','admin\UserController@create');
        Route::post('add','admin\UserController@store');

        Route::get('update/{id}','admin\UserController@edit');
        Route::post('update/{id}','admin\UserController@update');

        Route::get('delete/{id}','admin\UserController@destroy');

        Route::get('search','admin\UserController@search');
    });

    //bill
    Route::group(['prefix'=>'order'],function (){
        Route::get('display','admin\OrderController@index');

    });

    //products
    Route::group(['prefix'=>'product'],function (){
        Route::get('display','admin\ProductController@index')->name('product.display');

        Route::get('add','admin\ProductController@create');
        Route::post('add','admin\ProductController@store');

        Route::get('update/{id}','admin\ProductController@edit');
        Route::post('update/{id}','admin\ProductController@update');

        Route::get('delete/{id}','admin\ProductController@destroy');

        Route::get('search','admin\ProductController@search');
    });

    //image_products
    Route::group(['prefix'=>'imageproduct'],function (){
        Route::get('display','admin\ImageproductController@index');

        Route::get('add','admin\ImageproductController@create');
        Route::post('add','admin\imageproductController@store');

        Route::get('update/{id}','admin\ImageproductController@edit');
        Route::post('update/{id}','admin\ImageproductController@update');

        Route::get('delete/{id}','admin\ImageproductController@destroy');
    });

    //type_products
    Route::group(['prefix'=>'typeproduct'],function (){
        Route::get('display','admin\TypeproductController@index') ;

        Route::get('add','admin\TypeproductController@create');
        Route::post('add','admin\TypeproductController@store');

        Route::get('update/{id}','admin\TypeproductController@edit');
        Route::post('update/{id}','admin\TypeproductController@update');

        Route::get('delete/{id}','admin\TypeproductController@destroy');

        Route::get('search','admin\TypeproductController@search');
    });

    //news
    Route::group(['prefix'=>'new'],function (){
        Route::get('display','admin\NewController@index');

        Route::get('add','admin\NewController@create');
        Route::post('add','admin\newController@store');

        Route::get('update/{id}','admin\NewController@edit');
        Route::post('update/{id}','admin\NewController@update');

        Route::get('delete/{id}','admin\NewController@destroy');

        Route::get('search','admin\NewController@search');
    });

    //image_news
    Route::group(['prefix'=>'image_new'],function (){
        Route::get('display','admin\ImagenewController@index');

        Route::get('add','admin\ImagenewController@create');
        Route::post('add','admin\imagenewController@store');

        Route::get('update/{id}','admin\ImagenewController@edit');
        Route::post('update/{id}','admin\ImagenewController@update');

        Route::get('delete/{id}','admin\ImagenewController@destroy');

        Route::get('search','admin\ImagenewController@search');
    });

    //slide
    Route::group(['prefix'=>'slide'],function (){
        Route::get('display','admin\SlideController@index');

        Route::get('add','admin\SlideController@create');
        Route::post('add','admin\SlideController@store');

        Route::get('update/{id}','admin\SlideController@edit');
        Route::post('update/{id}','admin\SlideController@update');

        Route::get('delete/{id}','admin\SlideController@destroy');

        Route::get('search','admin\SlideController@search');
    });

    //dasboard
    Route::get('dasboard',function (){
        return view('admin.dasboard.dasboard') ;
    });

});
